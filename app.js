var express = require("express"),
    app = express(),
    proxy = require('http-proxy-middleware');

// CORS Header
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Set up Proxy for BEV
app.use('/GeoServer', proxy({target: 'https://wsa.bev.gv.at', changeOrigin: true}));

// start up the server
console.log('Listening on port: ' + 8080);
app.listen(8080); 
